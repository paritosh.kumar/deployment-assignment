import React from "react";

export default function Firbase() {
    return(
        <div>
            <h2>
                Firebase
            </h2>
            <p><a className="deploy-link" href="https://quantiphi-training.web.app/">Link</a></p>
            <ol>
          <li>
            Created react project
          </li>
          <li>
            Do <code>npm run build</code>
          </li>
          <li>
            Install firebase CLI using 
            <code>
              npm install -g firebase-tools
            </code>
          </li>
          <li>
            Login to the firbase using the CLI <code>
              firebase login
            </code>
          </li>
          <li>
            Initialize firebase for the project using <code>firbase init</code>. Choose Hosting option.
          </li>
          <li>
            Select <code>build</code> folder as the project directory.
          </li>
          <li>Run <code>firebase deploy</code> to deploy </li>
        </ol>
         
        </div>
    )
}