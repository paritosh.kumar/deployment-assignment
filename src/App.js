import React from 'react';
import './App.css';
import Firebase from './Firebase'
import Heroku from './Heroku'
import Ec2 from './Ec2'

function App() {
  // const [deploy, setDeploy] = React.useState(0)
  return (
    <div className="App">
      <header className="App-header">
        <h1>
        Paritosh Kumar 
        </h1>
        <h3>
          QuantID: 21061
        </h3>
        <p>Simple React App hosted on various platforms.</p>
        <h2>Steps taken</h2>
      </header>
      <div className="content">
      <Firebase />
      <Heroku />
      <Ec2 />
      </div>
      
    </div>
  );
}

export default App;
