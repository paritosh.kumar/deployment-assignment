import React from "react"

export default function Ec2() {
    return(
        <div>
            <h2>EC2/VM</h2>
            <p><a className="deploy-link" href="http://52.200.115.51/">Link</a></p>
            <p>
                Configuring VM or EC2 for serving static files or node application. 
            </p>
            <h3>Creating the EC2 Instance and assinging a static elastic IP address.</h3>
            <ol>
                
                <li>
                    Create an EC2 instance by clicking on <code>Launch Instance.</code>
                </li>
                <li>
                    Select <i>free tier only</i> checkbox for only free AMIs. I selected <b>Ubuntu Server 18.04 LTS (HVM), SSD Volume Type</b>
                </li>
                <li>Select <b>t2.micro</b> which is the free instance. </li>
                <li>Click on <b>Configure Security Group</b> for configuring the security policies for the instance. </li>
                <li>
                    Click <b>Add Rule</b>. Under <b>type</b> select HTTP and allow anywhere. This will create a new security group for our instance that will allow for HTTP traffic. 
                </li>
                <li>Then Click on preview and launch then create a new key pair or select old one. Download the pem file. Then finally click launch.</li>
                Now we will confiure a Elastic IP so that when instance refreshes or terminates or stops, its access IP address does not change.
                <li>Click on Allowcate Elastic IP. Then selecting this allocated IP click on actions then Associate this IP. Select the instance that was previously created and associate this elastic IP with this.</li>

            </ol>
        <h3>Configuring the instance.</h3>
            <ol>
                
                <li>Connect to the instance ussing SSH connecting by using the .pem file that we downloaded. I used git bash on windows for sshing.</li>
                    <ol>
                        <li>
                            <code>chmod 400 {'<path-to-key-file>'} </code> e.g. <code>chmod 400 ~/Downloads/my-aws-key.pem</code>
                        </li>
                        <li>Copy "Public DNS (IPv4)" property of the instance. Then connect using the <code>{'ssh -i <path-to-key-file> ubuntu@<domain name>'}</code> command. Enter yes for the next prompt.</li> 
                    </ol>
                <li>
                    The instance will need Nginx for serving all the static content. We will also need to install nodejs. Git is already pre installed so we can clone our repo here.
                </li>
                <li>The rest of the Nginx config can be seen from here <a href="https://www.nginx.com/blog/setting-up-nginx/#web-server">link</a></li>
                <li>For serving my react build files, I set the root directory in the <code>server1.conf</code> file as the directory of the build folder of react.</li>
                <li>To setup a Node server running forever and serving the app using an express server we can follow this tutorial (<a href="https://www.freecodecamp.org/news/production-fullstack-react-express/">Link</a>). </li>
            </ol>
        </div>
    )
}