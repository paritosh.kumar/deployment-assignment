import React from "react"
export default function Heroku() {
    return(
        <div>
            <h2>
                Herkou
            </h2>
            <p>
                <a className="deploy-link" href="https://immense-beyond-40907.herokuapp.com/">Link</a>
            </p>
            <ol>
                <li>
                    Initialize a git repository
                </li>
                <li>
                    Create new heroku app using the <code>heroku create</code> command. <code>heroku create -b https://github.com/mars/create-react-app-buildpack.git</code>.
                    The <code>-b</code> tag defines the build configuration that heroku will use for setting up the app.
                </li>
                <li>This adds a heroku remote for the repository.</li>
                <li>Pushing to <code>heroku master</code> pushes the repo directly to the heroku app which uses the build config and other files to create and configure the app.</li>
                <li><code>heroku open</code> opens the app.</li>
            </ol>
        </div>
    )
}