# Sample React App for deployment experiments

The detailed explanation can be found on the website itself.

- Firebase Link : [quantiphi-training.web.app](https://quantiphi-training.web.app/)
- Heroku Link : [immense-beyond-40907.herokuapp.com](https://immense-beyond-40907.herokuapp.com/)
- EC2 Link : [52.200.115.51](http://52.200.115.51/)